const {
    success,
    error
} = require('../utils/general');
const {
    APPLICATION_ERROR,
    NOT_FOUND,
    TIMER_END_WARNING,
    TIMER_START_WARNING
} = require('../models/Errors');
const TimeSheet = require('../models/TimeSheet');
const { Sequelize, Model, DataTypes, Op } = require('sequelize');

const getListTimeSheet = async (req, res) => {
    try {

        let innerQuery = { 
            uid : {
                [Op.eq]: req.user.id,
            }
        }

        if(req.query.keyword){
            let keywordBreak = req.query.keyword.split(' ')
            let listKeyword = keywordBreak.map(keyword => Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('title')), 'LIKE', '%' + keyword + '%'));
            innerQuery.title = {
                [Op.or] : listKeyword ? listKeyword : []
            }
        }

        let query = {
            where : {
                [Op.and]:innerQuery
            },
            order: [ ['type',  'ASC'], ['timestamp',  'DESC'] ]
        }

        const list = await await TimeSheet.findAll(query);
        return success({ timeslots : list ?? [] }, res);
    } catch (err) {
        return error(res,APPLICATION_ERROR);
    }
}

const updateTimeSheet = async (req, res, fastify) => {
    
    try {
        let queryMember = {
            where: {
                [Op.and] : {
                  uid : {
                      [Op.eq]: req.user.id,
                  },
                  id : {
                      [Op.eq]: req.params.id,
                  }
                }
            }
        }
        const selectedTimeSlot = await TimeSheet.findOne(queryMember);
    
        if(!selectedTimeSlot) return error(res,NOT_FOUND);
    
        let updateRecord = {};
    
        if(selectedTimeSlot.title !== req.body.title){
            updateRecord.title = req.body.title
        }
    
        if(selectedTimeSlot.timestamp !== req.body.timestamp){
            updateRecord.timestamp = new Date(req.body.timestamp).toISOString().slice(0, 19).replace('T', ' ');
        }

        if(selectedTimeSlot.type !== req.body.type){
            updateRecord.type = req.body.type
        }
        
        await TimeSheet.update(updateRecord,queryMember)

        return success(true, res);
    } catch (err) {
        return error(res,APPLICATION_ERROR);
    }
}

const addTimeSheet = async (req, res, fastify) => {
    try {

        let queryMember = {
            where: {
                [Op.and] : {
                  uid : {
                      [Op.eq]: req.user.id,
                  },
                }
            },
            order: [ ['id',  'DESC']]
        }
        const selectedTimeSlot = await TimeSheet.findOne(queryMember);

        if(!selectedTimeSlot){
            if(req.params.type === 'stop'){
                return error(res, TIMER_START_WARNING);
            }
        }

        if(selectedTimeSlot){
            if(req.params.type === selectedTimeSlot.type){
                return error(res, req.params.type === 'start' ? TIMER_END_WARNING : TIMER_START_WARNING);
            }
        }

        await TimeSheet.create({
            uid : req.user.id,
            title : req.body.title,
            type : req.params.type,
        })
        
        return success(true, res);
    } catch (err) {
        return error(res,APPLICATION_ERROR);
    }
}


module.exports = {
    addTimeSheet, 
    updateTimeSheet, 
    getListTimeSheet
}