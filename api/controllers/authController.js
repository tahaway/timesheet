const { SignUpResponse } = require('../models/Auth');
const { INVALID_PASSWORD, USER_DOESNT_EXISTS, USER_EXISTS } = require('../models/Errors');
const { VERIFICATION_EMAIL_SENT } = require('../models/Messages');
const { getRandomBytes,createHash, compareHash } = require('../utils/crypto');
const { error,success } = require('../utils/general');
const nconf = require('nconf');
const User = require('../models/User');
const TimeSheet = require('../models/TimeSheet');
const { objectID  } = require('../utils/general');
const { Op } = require("sequelize");

const postLogin = async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({
            where: {
              email: {
                [Op.eq]: email.toLowerCase().toString(),
              }
            }
        });
        if (!user) return error(res,USER_DOESNT_EXISTS);
        const isMatch = await compareHash(password,user.password);
        if(!isMatch)  return error(res,INVALID_PASSWORD);

        const { id } = user;
        const token = await res.jwtSign({ id }, { expiresIn: nconf.get('app.userJwtExpiry') });
        const response = { user : { email, token, id } };
        return success(response,res);
    } catch (err) {
        return error(res);
    }
};

const postSignup = async (req, res) => {
    const { email, password } = req.body;
    try {
        const existingUser = await User.findOne({
            where: {
              email: {
                [Op.eq]: email.toLowerCase().toString(),
              }
            }
        });
        if (existingUser) return error(res,USER_EXISTS);
        const passwordHash = createHash(password);
        const newUser = await User.create({ email, password : passwordHash, status : true });
        const { id } = newUser;
        const token = await res.jwtSign({ id }, { expiresIn: nconf.get('app.userJwtExpiry') });
        return success({ user : { email: email, token, id } },res)
    } catch (err) {
        return error(res);
    }
};

module.exports = {
    postLogin,
    postSignup,
};