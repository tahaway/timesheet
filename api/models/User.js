const {
  Sequelize,
  Model,
  DataTypes
} = require('sequelize');
const {
  getRandomBytes,
  createHash,
  compareHash
} = require('../utils/crypto');
const nconf = require('nconf');
const sequelize = new Sequelize('sqlite::memory:', nconf.get('sequelize.config'));

class User extends Model {}

User.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  email: DataTypes.STRING,
  password: DataTypes.STRING,
  status: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
}, {
  sequelize,
  modelName: 'user'
});

(async () => {
  await sequelize.sync({
    logging: false,
    force: true
  });

})();


module.exports = User;