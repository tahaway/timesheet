const APPLICATION_ERROR = "API facing problem to handle this request.";
const USER_EXISTS = 'Email address already exists.';
const INVALID_PASSWORD = 'Invalid Password.';
const USER_DOESNT_EXISTS = 'User doesn\'t exist.';
const NOT_FOUND = "Record not found.";
const INVALID_TOKEN = 'Invalid or Expired Token.';
const NOTHING_UPDATE = "Nothing to update.";
const TIMER_START_WARNING = "Timer session not found, please start new session to stop";
const TIMER_END_WARNING = "Timer session is already running, please stop old session to start new timer.";


module.exports = {
  USER_EXISTS,
  INVALID_PASSWORD,
  USER_DOESNT_EXISTS,
  INVALID_TOKEN,
  NOTHING_UPDATE,
  APPLICATION_ERROR,
  NOT_FOUND,
  TIMER_START_WARNING,
  TIMER_END_WARNING
};