const {
  Sequelize,
  Model,
  DataTypes
} = require('sequelize');
const {
  getRandomBytes,
  createHash,
  compareHash
} = require('../utils/crypto');
const nconf = require('nconf');
const sequelize = new Sequelize('sqlite::memory:', nconf.get('sequelize.config'));

class TimeSheet extends Model {}

TimeSheet.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  uid: DataTypes.INTEGER,
  title: DataTypes.STRING,
  type: {
    type: DataTypes.ENUM,
    values: ['start', 'stop'],
    defaultValue: 'start'
  },
  timestamp: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updatedAt: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  }, {
  sequelize,
  modelName: 'TimeSheet'
});

(async () => {
  await sequelize.sync({
    force: true
  });
})();

module.exports = TimeSheet;