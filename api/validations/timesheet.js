const {
    code400,
    code401,
    code409,
    code200GetListTimeSheet,
    code200SuccessTimeSheet
} = require('../../config/swagger')

const validateAddTimeSheet = {
    schema: {
        description: 'Start/Stop your time slot on timesheet.',
        tags: ['timesheet'],
        body: {
            type: 'object',
            properties: {
                title: {
                    type: 'string',
                }
            },
            required: ['title'],
        },
        params: {
            type: 'object',
            properties: {
                type: {
                    type: 'string',
                    enum: ['start', 'stop'],
                    default: 'start',
                }
            },
            required: ['type'],
        },
        response: {
            400: code400,
            401: code401,
            200: code200SuccessTimeSheet,
            409: code409
        }
    }
}

const validateUpdateTimeSheet = {
    schema: {
        description: 'Update timer slot entries on timesheet.',
        tags: ['timesheet'],
        params: {
            type: 'object',
            properties: {
                id: {
                    type: 'number',
                }
            },
            required: ['id'],
        },
        body: {
            type: 'object',
            properties: {
                type: {
                    type: 'string',
                    enum: ['start', 'stop'],
                    default: 'start',
                },
                title: {
                    type: 'string',
                    min: 3,
                },
                timestamp: {
                    type: 'string',
                },
            },
            required: ['type', 'title', 'timestamp'],
        },
        response: {
            400: code400,
            401: code401,
            200: code200SuccessTimeSheet,
            409: code409
        }
    }
}

const validateListTimeSheet = {
    schema: {
        description: 'List all timeslots from timesheet.',
        tags: ['timesheet'],
        query: {
            type: 'object',
            properties: {
                keyword: {
                    type: 'string',
                }
            }
        },
        response: {
            400: code400,
            401: code401,
            200: code200GetListTimeSheet,
            409: code409
        }
    }
}


module.exports = {
    validateListTimeSheet,
    validateUpdateTimeSheet,
    validateAddTimeSheet
}