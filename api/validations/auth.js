const nconf = require('nconf');
const {
  randomID
} = require('../utils/general');
const userPasswordRegex = nconf.get('userPasswordRegex');
const {
  code400,
  code401,
  code409,
  code200Token
} = require('../../config/swagger');

const validatePostLogin = {
  schema: {
    description: 'Authenticate with your account to obtain access token.',
    tags: ['auth'],
    body: {
      type: 'object',
      properties: {
        email: {
          type: 'string',
          format: 'email'
        },
        password: {
          type: 'string',
          format: 'regex',
          pattern: userPasswordRegex
        },
      },
      required: ['email', 'password'],
    },
    response: {
      400: code400,
      401: code401,
      200: code200Token,
      409: code409
    },
  },
};

const validatePostSignup = {
  schema: {
    description: 'Register account to use CDN API.',
    tags: ['auth'],
    body: {
      type: 'object',
      properties: {
        email: {
          type: 'string',
          format: 'email'
        },
        password: {
          type: 'string',
          format: 'regex',
          pattern: userPasswordRegex,
          minLength: 6,
          maxLength: 20,
        },
      },
      required: ['email', 'password'],
    },
    response: {
      400: code400,
      401: code401,
      200: code200Token,
      409: code409
    },
  },
};

module.exports = {
  validatePostLogin,
  validatePostSignup,
};