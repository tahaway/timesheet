const crypto = require('crypto');
const bcrypt = require('bcryptjs');

const getRandomBytes = size => crypto.randomBytes(size).toString('hex');

const createHash = password => bcrypt.hashSync(password, 10);

const compareHash = (password, hash) => bcrypt.compareSync(password, hash);


module.exports = {
  getRandomBytes,
  createHash,
  compareHash
};