const request = require('request');
const {  addTimeSheet, updateTimeSheet, getListTimeSheet } = require('../controllers/timeSheetController')
const {  validateListTimeSheet, validateUpdateTimeSheet, validateAddTimeSheet } = require('../validations/timesheet');

module.exports = async function (fastify,opts) {

    fastify.route({
        method: 'POST',
        url: '/v1/api/timesheet/action/:type/',
        schema : validateAddTimeSheet.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler : addTimeSheet
    })

    fastify.route({
        method: 'POST',
        url: '/v1/api/timesheet/update/:id/',
        schema : validateUpdateTimeSheet.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler : updateTimeSheet
    })

    fastify.route({
        method: 'GET',
        url: '/v1/api/timesheet/list/',
        schema : validateListTimeSheet.schema,
        preHandler: fastify.auth([
            fastify.asyncVerifyJWT,     
        ]),
        handler : getListTimeSheet
    })
    

  }