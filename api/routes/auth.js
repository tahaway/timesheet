const { validatePostLogin, validatePostSignup } = require('../validations/auth');
const { postLogin, postSignup } = require('../controllers/authController');

module.exports = async (fastify) => {
  fastify.post('/v1/api/auth/login/', validatePostLogin, postLogin);
  fastify.post('/v1/api/auth/signup/', validatePostSignup, postSignup);
};
