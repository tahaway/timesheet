'use strict'
const {
    test
} = require('tap')
const nconf = require('nconf');
const supertest = require('supertest')
const build = require('./server')
const {
    loadSettings
} = require('./config/configurationAdaptor')
const appSettingsPath = "./config/appSettings.json"

test('requests the "/v1/api/timesheet/list/" route', async t => {

    loadSettings({
            appSettingsPath
        })
        .then(async () => {
            // Read the config property required for starting the server

            const serverOptions = {
                app: nconf.get('app'),
                fastify: nconf.get('fastify'),
                logging: nconf.get('logging'),
            };

            const fastify = await build.createServer(serverOptions);
            t.tearDown(() => fastify.close())
            const response = await supertest(fastify.server)
                .get('/')
                .expect(404)
                .expect('Content-Type', 'application/json; charset=utf-8')

            const newrequest = await supertest(fastify.server)
                .get('/v1/api/timesheet/list/')
                .expect(401)
                .expect('Content-Type', 'application/json; charset=utf-8')
        })
})