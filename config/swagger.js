const {
  randomID,
} = require('../api/utils/general');
var md5 = require('md5');

const code400 = {
  description: 'Invalid body payload.',
  type: 'object',
  properties: {
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    statusCode: 400,
    error: 'Bad Request',
    status: 'fail',
    message: 'Body message error',
  }
};

const code401 = {
  description: 'Unauthorized response',
  type: 'object',
  properties: {
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    statusCode: 401,
    error: 'Unauthorized',
    message: 'Unauthorized Token Error',
  }
}

const code409 = {
  description: 'Failure response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('failure'),
    status: 'fail',
    error: 'Conflict',

    message: 'Error Message',
  }
}

const code200SuccessTimeSheet = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    }
  },
  example: {
    id: randomID('request'),
    status: 'success'
  }
}


const code200GetListTimeSheet = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    timeslots: {
      type: 'array',
    }
  },
  example: {
    id: randomID('request'),
    status: 'success',
    timeslots: [{
      id: 1,
      title: 'testing',
      type: 'start',
      timestamp: false
    }]
  }
}


const code200Message = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    message: 'Message for operation',
  }
}

const code200Token = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    user: {
      type: 'object',
      properties: {
        id: {
          type: 'string'
        },
        email: {
          type: 'string'
        },
        token: {
          type: 'string'
        },
      }
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    user: {
      id: md5(new Date() + 7000),
      token: Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2),
      email: 'user@domain.com',
    }
  }
}




module.exports = {
  code400,
  code401,
  code409,
  code200Message,
  code200Token,
  code200GetListTimeSheet,
  code200SuccessTimeSheet
};