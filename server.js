require('dotenv').config();
const Fastify = require('fastify');
const path = require('path');
const AutoLoad = require('fastify-autoload');


const createServer = (options) => {
    
    const {
        logging,
        app,
        fastify,
        swagger
    } = options;

    if(process.env.HOST && process.env.PORT){
        app.host = process.env.HOST
        app.port = process.env.PORT
    }

    // create the server
    const server = Fastify({
        logger: logging ? fastify.config.logs : logging,
        ignoreTrailingSlash: true,
        useUnifiedTopology: true,
    });

    
    server
        .register(require('fastify-swagger'), {
            routePrefix: '/documentation',
            swagger: swagger,
            exposeRoute: true
        })

    server
        .register(require('fastify-jwt'), {
            secret: app.jwtKey,
        })

    server
        .decorate('asyncVerifyJWT', async function (request, reply, next) {
            try {
                await request.jwtVerify();
            } catch (err) {
                reply.send(err)
            }
        })
        .register(require('fastify-auth'));

    server
        .register(AutoLoad, {
            dir: path.join(__dirname, 'api', 'routes')
        });

    // start the server
    server.listen(app.port, app.host, (err) => {
        if (err) {
            server.log.error(err);
            console.log(err);
            process.exit(1);
        }
        console.log("API End-point is up at http://" + app.host + ":" + app.port);
        console.log("API Swagger Documentation at http://" + app.host + ":" + app.port + '/documentation/');

        server.log.info('Server Started');
    });
    return server;
}

module.exports = {
    createServer
}